const apiKey = "ad93ec9d6bcf5c35eec3b3f597b82047";
const apiUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&q=";
const btn = document.getElementById("search");





async function checkWether(city) {



    const response = await fetch(apiUrl + city + `&appid=${apiKey}`);
    
    // console.log(data);

    // console.log(data.main.humidity);

    // console.log(data.weather[0].main);

    if (response.status == 404) {
       document.querySelector('.error').style.display="block";
       document.querySelector('.weather').style.display="none"

    }
    else {
        document.querySelector('.error').style.display="none";
        var data = await response.json();

        const condition = data.weather[0].main;

        const img = document.querySelector(".weather img");
        console.log(img);

        document.querySelector(".city").innerHTML = data.name;
        document.querySelector('.temp').innerHTML = data.main.temp + "°C";
        document.querySelector('.wind').innerHTML = data.wind.speed + "km/hr"
            ;

        document.querySelector('.humidity').innerHTML = data.main.humidity + "%"

        if (condition == "Clouds") {

            img.src = "./img/clouds.png"
        }
        else if (condition == "Haze") {
            img.src = "img/haze.png"
        }
        else if (condition == "Drizzle") {
            img.src = "img/drizzle.png"
        }
        else if (condition == "Rain") {
            img.src = "img/rain.png"
        }
        else if (condition == "Mist") {
            img.src = "img/mist.png"
        }
        else if (condition == "Snow") {
            img.src = "img/snow.png"
        }
        else if (condition == "Clear") {
            img.src = "img/clear.png"
        }
        document.querySelector(".main").innerHTML=condition;
        document.querySelector('.weather').style.display="block"

    }





}

btn.addEventListener('click', () => {
    const city = document.querySelector('.search input')
    console.log(city.value);

  if(city.value==""){

    alert("Enter a valid city to proceed")

  }else{
    checkWether(city.value);
    city.value="";

  }

   
});

document.querySelector('.weather').style.display="none";

